# 发布应用到 KubeSphere 的 App Store

首先进入到 `应用管理 - 应用模版界面`，找到已经上传的应用模版。

可以看到应用的状态是开发中的状态。

![appstore1](pic/appstore1.png)

进入详情页面，可以看到应用是待提交页面。

![appleestore2](pic/appstore2.png)

下拉页面选择 `提交审核`。

![appstore3](pic/appstore3.png)

进入提交审核的测试步骤。

![appstore4](pic/appstore4.png)

点击下一步，提交更新日志。

![appstore5](pic/appstore5.png)

点击确定后，应用状态变为等待审核。

![appstore6](pic/appstore6.png)

在下拉菜单中查看应用的详细信息

![appstore7](pic/appstore7.png)

使用 admin 账号，选择应用商店管理页面。

![appstore8](pic/appstore8.png)

在`应用审核`位置可以发现待审核的应用。

![appstore9](pic/appstore9.png)

点击右侧三个点，选择审核。

![appstore10](pic/appstore10.png)

在审核页面选择通过。

![appstore11](pic/appstore11.png)

查看应用审核页面，发现应用已经上架。

![appstore12](pic/appstore12.png)

回到应用界面，选择 `发布到商店。`

![appstore13](pic/appstore13.png)

确定提示信息后，应用发布到商店。用户可以查看并部署该应用版本。

![appstore14](pic/appstore14.png)

发布完成后，应用的状态变更为 `已上架`。

![appstore15](pic/appstore15.png)

此时可以在应用商店中查询到刚部署的应用。

![appstore16](pic/appstore16.png)

同时，点击进应用可以发现新部署应用的详情信息。

![appstore17](pic/appstore17.png)

至此，已经实现将 APISIX 应用发布到 KubeSphere 的 App Store 中。


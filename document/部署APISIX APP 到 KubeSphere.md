# 部署APISIX APP 到 KubeSphere

在完成前述的环境配置后，可以按照以下的步骤构建 APISIX 应用。

### 1. 本地构建 APISIX APP

```shell
> git clone https://github.com/kubesphere/helm-charts
> cd helm-charts/src/test
> helm dependency update apisix
Saving 1 charts
Downloading etcd from repo https://charts.kubesphere.io/test
Deleting outdated charts

> helm package apisix
Successfully packaged chart and saved it to: */helm-charts/src/test/apisix-0.1.7.tgz
```

### 2. 安装 APISIX APP 到 KubeSphere

在如下位置，使用已经获得的 APISIX APP 包创建模版。

![image-20210811210547084](pic/create-template1.png)

![create-template2](pic/create-template2.png)

上传 APISIX APP 生成模版

![create-template3](pic/create-template3.png)

创建成功

![create-template4](pic/create-template4.png)

### 3. 部署应用到 KubeSphere 平台

点击应用进入 apisix 信息页面，选择部署。

![deploy1](pic/deploy1.png)

首先是部署基本信息页面。

![deploy2](pic/deploy2.png)

点击下一步，进入到应用配置界面。

在配置页面可以根据实际应用的需要微调细节的配置项。

![deploy3](pic/deploy3.png)

回到项目主页面，发现应用已经成功部署。

![deploy-success1](pic/deploy-success1.png)

应用运行正常。

![deploy-success2](pic/deploy-success2.png)